**To Run the application**

{path}/target/java -jar SpringBootDockerAssessment-0.0.1-SNAPSHOT.jar

**Build image**

1.{Dockerfile path}/docker build -t dockerapplication .

2.{Dockerfile path}/docker run -p8080:8080 dockerapplication

**To access the API**

1.http://localhost:8080/fibonnaciSeries/10

2.http://localhost:8080/factorial/4
