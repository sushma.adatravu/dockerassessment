package com.example.SpringBootDockerAssessment.Controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DockerController {

    @RequestMapping(value = "/fibonnaciSeries/{n}",method = RequestMethod.GET)
    public int[] fibonnaciSeries(@PathVariable int n) {
        int n1 = 0, n2 = 1, n3, i;
        System.out.print(n1 + " " + n2);
        int[] series = new int[n];
        series[0] = n1;
        series[1] = n2;
        for (i = 2; i < n; ++i)
        {
            n3 = n1 + n2;
            System.out.print(" " + n3);
            series[i] = n3;
            n1 = n2;
            n2 = n3;
        }
        return series;
    }

    @RequestMapping(value = "/factorial/{n}",method = RequestMethod.GET)
        public int factorial(@PathVariable int n){
            int i,fact=1;
            for(i=1;i<=n;i++){
                fact=fact*i;
            }
            System.out.println("Factorial of "+n+" is: "+fact);
             return fact;
        }
}
